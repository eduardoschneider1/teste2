package br.edu.iftm.ads.projetopilha;

import br.edu.iftm.ads.projetopilha.excecoes.EstruturaVaziaException;

public class PilhaLDL<T extends Comparable<T>> implements Pilha<T> {

	private ListaDuplamenteLigada<T> dados;

	public PilhaLDL() {
		dados = new ListaDuplamenteLigada<T>();
	}

	@Override
	public void push(T valor) {
		dados.insereInicio(valor);
	}

	@Override
	public T pop() throws EstruturaVaziaException {
		T inicio = dados.getInfoInicio();
		dados.removeInicio();
		return inicio;
	}
	@Override
	public String toString() {
		return dados.toString();
	}
}
